This is a take-home assignment for [Ferriswheel](https://www.ferriswheel.io).

# Instructions

1. Download the contents of this github

```bash
git clone https://gitlab.com/psuchand/datascience-interview-ferriswheel
```

2. Load assignment.ipynb in a jupyter notebook, and run all of the cells by selecting the "Restart & Run All" option. This will take some time, because the notebook will scrape content from imdb.com

3. After the notebook has finished processing, scroll to the bottom to the section titled "Example Searches". There you will see example searches for "spielberg hanks" and "spielberg". Modify them as you like.

## Notes

1. The top 1K movies are defined according to the list of "The 1000 Greatest Films of All Time", taken from here: https://www.imdb.com/list/ls006266261

#  Simplifying Assumptions

For simplicity, we made the following assumptions:
1. We assumed that the user will only search for a movie by specifying its director, producer, or member of the cast.
2. We assumed that each word of the search query contains only lowercase alphanumeric characters, and is at least three characters. 
3. Implicit And: If a user searches for "word1 word2" we return ```search("word1") intersect search("word2")```. Simlarly for search strings with multiple words.

# Further Improvements

This is simple code written in a jupyter notebook. We could use Elasticsearch (see below). One could also write the code in pure Python as follows:

1. A python script that will scrape IMDB, and produce a JSON file for each movie.
2. Index each term that appears in the document. The index can be spread across many files and updated/accessed in a thread safe manner by using multiple partitions via a hash function.
3. The search function would be very similar to what we have now, but it would be aware of the number of partitions, and the hash function. Also, the search function will only have to query the partitions relevant for the given search phrase.

## Quality

1. Allow for phrase search (eg, allow for searching for the string "steven spielberg") which may be more specific in case there are two directors with the last name spielberg. Note that currently the index looks like this:

keyword -> {movie indices}

In order to implement this, we'd modify the index to look like:

keyword -> {(movie index, index of keyword in movie document)}

When searching for phrases, we'd check to make sure that the keywords appear in the documents in the same order as in the search phrase.

2. Index the descriptions of the movies. Now, don't index each word - but rather first stem the word and index that (eg, remove plurals).

## Elasticsearch
For scale, we'd prefer to use Elasticsearch as follows.

1. A python script that will scrape IMDB, and produce JSON.
2. We'd ingest the JSON data into Elasticsearch, and index along all of the keys.
3. Use the Elasticsearch web interface to search the ingested data.